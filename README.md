Website
=======
http://www.zlib.net/

License
=======
zlib license (see the file source/README)

Version
=======
1.2.10

Source
======
zlib-1.2.10.tar.xz (sha256: 9612bf086047078ce3a1c154fc9052113fc1a2a97234a059da17a6299bd4dd32)
